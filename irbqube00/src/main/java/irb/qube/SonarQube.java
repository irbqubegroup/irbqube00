package irb.qube;



/**
 * Hello world!
 *
 */
public class SonarQube
{
    private static class SonarQubeHelper {
        static final String SONAR_QUBE_HELPER_STRING = "Sonar Qube Helper";

        static String getSonarQubeHelperString() {
            return SONAR_QUBE_HELPER_STRING;
        }
    }

    int i = 0;
    int j;

    public static void main( String[] args )
    {
        SonarQube sonarQube = new SonarQube();

        System.out.format("%d %d%n", sonarQube.i, sonarQube.j);
        System.out.println( "Hello World!" );

        System.out.format("%s %n", SonarQubeHelper.getSonarQubeHelperString());
    }
}
